import { Action, Selector, State, StateContext } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { AddTodo, EmptyTodo, MarkDone } from './todo.actions';

// export interface ITodo {
//   id: string;
//   name: string;
//   is_done: boolean;
// }

export interface TodoStateModel {
  todoList: string[];
  is_done: boolean[];
}

@State<TodoStateModel>({
  name: 'todo',
  defaults: {
    todoList: [],
    is_done: [],
  }
})
export class TodoState {

  @Selector()
  static getTodoList(state: TodoStateModel): string[] {
    return state.todoList;
  }

  @Selector()
  static getIsDoneList(state: TodoStateModel): boolean[] {
    return state.is_done;
  }

  @Action(AddTodo)
  addTodo({ patchState, getState }: StateContext<TodoStateModel>, { newTodo, is_done }: AddTodo): void {
    patchState({ todoList: [...getState().todoList, newTodo], is_done: [...getState().is_done, is_done] });
  }

  @Action(EmptyTodo)
  emptyTodo({ patchState }: StateContext<TodoStateModel>): void {
    patchState({ todoList: [] , is_done: []});
  }

  @Action(MarkDone)
  onCheckedBox({ patchState, getState, setState }: StateContext<TodoStateModel>, { is_done }: MarkDone): void {
    patchState({ is_done: [...getState().is_done, is_done] });
    //setState(patch({is_done: updateItem((item: ITodo) => item.id === payload, patch({ is_done: !is_done }))}));
  }
}
