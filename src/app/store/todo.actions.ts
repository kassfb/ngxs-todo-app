import { Observable } from "rxjs/internal/Observable";

export class AddTodo {
  static readonly type = '[Todo] Add';
  constructor(public newTodo: string, public is_done: boolean) { }
}

export class MarkDone {
  static readonly type = '[Todo] MarkDone';
  constructor(public is_done: boolean) { }
}

export class EmptyTodo {
  static readonly type = '[Todo] Empty';
}
