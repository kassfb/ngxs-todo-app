import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AddTodo, EmptyTodo, MarkDone } from '../store/todo.actions';
import { TodoState } from '../store/todo.state';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @Select(TodoState.getTodoList) todoList$?: Observable<string[]>;
  @Select(TodoState.getIsDoneList) isDoneList$!: Observable<boolean[]>;

  private isDoneSubscription!: Subscription;

  newTodo = '';
  is_done = false;
  is_donee!: boolean[];
  constructor(private readonly store: Store) {

  }
  ngOnDestroy(): void {
    this.isDoneSubscription.unsubscribe();
  }

  onAddTodo(): void {
    if (this.newTodo.length > 0) {
      this.store.dispatch(new AddTodo(this.newTodo, this.is_done));
    }
    this.newTodo = '';
  }

  onEmptyList(): void {
    this.store.dispatch(new EmptyTodo());
  }

  onCheckedBox(is_checked: boolean): void {
    if(!is_checked) is_checked= false;
    this.store.dispatch(new MarkDone(is_checked));
  }
  
  // onCheckedBox(is_checked: boolean[]): void {
  //   this.isDoneSubscription = this.isDoneList$.subscribe((is_done: boolean[]) => {
  //     this.is_donee = is_done;
  //   });
  //   this.store.dispatch(new MarkDone(is_checked));
  // }

  mock(): void {
    console.log("mock");
  }
}
